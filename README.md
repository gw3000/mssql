# Restore MSSQL Databases (RAMICRO and raEloakte)

How to restore mssql databases into docker based mssql server.

## Drop raEloakte

```sql
DROP DATABASE raEloakte
GO
```

## Restore raEloakte from Backup

```sql
RESTORE DATABASE raEloakte
FROM DISK = '/tmp/raEloakte.bak'
WITH MOVE 'raEloakte' TO '/var/opt/mssql/data/raEloakte.mdf',
MOVE 'raEloakte_LOG' TO '/var/opt/mssql/data/raEloakte.ldf'
GO
```

## Drop RAMICRO

```sql
DROP DATABASE RAMICRO
GO
```

## Restore RAMICRO from Backup

```sql
RESTORE DATABASE RAMICRO
FROM DISK = '/tmp/RAMICRO.bak'
WITH MOVE 'RAMICRO' TO '/var/opt/mssql/data/RAMICRO.mdf',
MOVE 'RAMICRO_LOG' TO '/var/opt/mssql/data/RAMICRO.ldf'
GO
```
